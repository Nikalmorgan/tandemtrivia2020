# Author: Nikal Morgan

## Overview

At Tandem, we love to learn and have fun and what better way to do that than to play a round of trivia. One of our favorite ways to wait while our test suite is running is to hop over to the #games channel on Slack and play a quick round of trivia. Playing trivia isn’t just a fun way to learn something new but also a great way to take a little break from a normal work task.
While we’re all knowledgeable in our own right, only one person can be crowned trivia champion with the highest score.

## Goal

Your goal is to create an application that displays trivia questions with multiple-choice answers to select from.
Use creative license in terms of how you want us to see this game. At minimum, the player can view the question(s), the answer choices, the correct answer upon submission, and their score. It could be a user interface (UI), command-line-tool, etc. Feel free to use whatever framework or language you are comfortable with.
We would also like to see a README which includes any information about how to run the code, any known issues or complexity we should look out for, and any additional features you would like to have added to make your trivia app even more awesome.

## Key Criteria

- A round of trivia has 10 Questions
- All questions are multiple-choice questions
- A user can view questions.
- Questions with their multiple choice options must be displayed one at a time. Questions should not repeat in a round.
- A user can select only 1 answer out of the 4 possible answers.
- The correct answer must be revealed after a user has submitted their answer A user can see the score they received at the end of the round