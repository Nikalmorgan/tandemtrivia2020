// Global variables
let gameArea = document.getElementById("gameArea") //grab game area
let start_play = document.getElementById("selection") //grab selection
let info = document.getElementById("info") //grab info
let results = document.getElementById("results") //grab modal body
const question = document.getElementById("question")
const a1 = document.getElementById("1") //grab 1 input
const a2 = document.getElementById("2") //grab 2 input
const a3 = document.getElementById("3") //grab 3 input
const a4 = document.getElementById("4") //grab 4 input
const show = document.getElementById("show") //grab show div
const the_answers = document.getElementsByName('answers'); //grab answers input by name
const holder = document.getElementById("holder") //grab holder for results so show above points
const pts = document.getElementById("pp") //grab points span
const pname = document.getElementById("pn") //grab player span
const moves = document.getElementById("moves") //grab moves span
let moveNum = document.querySelector(".moves") //grabs moves div
let mode = false //here false means put down and true means pick up
let playing = false 
let randQ = 0
let points = 0
let question_count = 0
let all_questions = []
let question_data = []
let answer_data = []
let correct = ""

//below is only happens when page first loads; this function adds the selected disc number to screen
document.getElementById("selected").onclick = function(){
    playing = !playing
    start_play.style.display = 'none'; //turn display off
    info.style.display = 'none'; //turn display off
    gameArea.style.display = 'block'; //turn display back on
    holder.style.display = 'block'; //turn display back on
    moveNum.style.display = "block"; //turn display back on
    let playername = document.getElementById("player").value; //grabs select tag by id
    pname.innerHTML = playername;
    questionsNum = parseInt(moves.innerHTML); 
    question_count = questionsNum;
    console.log(question_count);

    if(playing){
        fetch('tandem2020.json')
        .then(response => response.json())
        .then(data => {
            console.log(data);
            play(data);
        });
    }
}

//below is listening for reset match button; 
//this function empties towers; 
//adds the previous disc  number to screen
document.getElementById("resetGame").onclick = function(){
    let results = document.createElement('p')
    results.innerHTML ="It is sometime wise to return to the beginning"
    results.style.color = "yellow"
    results.style.width = "40%"
    holder.prepend(results)
    //below allows the changes above to be seen for 2.25 secs before clearing
    //and re-adding previous selected disc amount
    setTimeout(function(){ 
        holder.removeChild(results)
        pname.innerHTML = "";
        pts.innerHTML = "";
        moves.innerHTML = 10;
    }, 5000);
}

//below is listening for end game button;
// this function reloads page
document.getElementById("endGame").onclick = function(){
    playing = false
    let results = document.createElement('p')
    results.innerHTML ="Sometimes we must end a mission before it ends us"
    results.style.color = "yellow"
    results.style.width = "40%"
    holder.prepend(results)
    //below allows the changes above to be seen for 2.25 secs before reloading page
    setTimeout(function(){ 
        holder.removeChild(results)
        resetWindow()
    }, 5000);
}

//Below answer choices div is listening for click event 
// check.addEventListener('click', () => {
//     mode = !mode //change mode

// })

//Below is the function for ending the game 
function resetWindow(){
    location.reload()
}

//Below is the function for playing the game 
function play(questions){
    all_questions = questions
    console.log(all_questions)
    console.log(all_questions.length)
    randQ = Math.floor(Math.random()* all_questions.length);
    console.log(randQ)
    question_data.push(all_questions[randQ])
    question.innerHTML = all_questions[randQ]["question"]
    correct = all_questions[randQ]["correct"]
    console.log(correct)
    answer_data.push(all_questions[randQ]["correct"])
    for(let i  = 0; i < all_questions[randQ]["incorrect"].length; i++){
        answer_data.push(all_questions[randQ]["incorrect"][i])
    }
    let randA = Math.floor(Math.random() * answer_data.length);
    a1.innerHTML = answer_data[randA]
    answer_data.splice(answer_data.indexOf(answer_data[randA]), 1);
    let randB = Math.floor(Math.random() * answer_data.length);
    a2.innerHTML = answer_data[randB]
    answer_data.splice(answer_data.indexOf(answer_data[randB]), 1);
    let randC = Math.floor(Math.random() * answer_data.length);
    a3.innerHTML = answer_data[randC]
    answer_data.splice(answer_data.indexOf(answer_data[randC]), 1);
    let randD = Math.floor(Math.random() * answer_data.length);
    a4.innerHTML = answer_data[randD]
    answer_data.splice(answer_data.indexOf(answer_data[randD]), 1);
}

//Below is th function for checking if a question is correct or incorrect!
document.getElementById("check").onclick = function(){  
    question_count -= 1; 
    console.log(question_count)
    if(question_count === -1){
        playing = false
        gameArea.style.visibility = 'hidden'; //turn visibility off
        let results = document.createElement('p')
        results.innerHTML = `<h3 style="font-weight: bolder; text-align: center;">Game Over!!!</h3>`
        results.style.color = "red"
        results.style.width = "40%"
        holder.prepend(results)
        //below allows the changes above to be seen for 2.25 secs before reloading page
        setTimeout(function(){ 
            holder.removeChild(results)
            resetWindow()
        }, 10000);
    } else {
        for(i = 0; i < the_answers.length; i++) { 
            console.log(the_answers[i].checked)
            if(the_answers[i].checked){
                console.log(the_answers[i].nextElementSibling.textContent)
                if(the_answers[i].nextElementSibling.textContent === correct){
                    moves.innerHTML = question_count
                    show.innerHTML = `Correct! ${question_data[question_data.length - 1]['question']} answer was ${correct}.`
                    points += 10
                    pts.innerHTML = `${points}`
                }else{
                    moves.innerHTML = question_count
                    show.innerHTML = `Incorrect! ${question_data[question_data.length - 1]['question']} answer was ${correct}`
                    points -= 10
                    pts.innerHTML = `${points}`
                }
                setTimeout(console.log("waiting"), 3000)
                // show.innerHTML = ""
                question.innerHTML = ""
                a1.innerHTML = ""
                a2.innerHTML = ""
                a3.innerHTML = ""
                a4.innerHTML = ""
                console.log(all_questions[randQ])
                // console.log(question_data)
                // question_data.pop()
                // console.log(question_data)
                all_questions.splice(randQ, 1)
                console.log(all_questions.length)
                play(all_questions)
            }
        }
        // alert('Resetting Entire Game! No Cheating!')
        // resetWindow();

    }        

}